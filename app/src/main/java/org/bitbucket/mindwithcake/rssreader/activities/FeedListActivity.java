package org.bitbucket.mindwithcake.rssreader.activities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import org.bitbucket.mindwithcake.rssreader.R;
import org.bitbucket.mindwithcake.rssreader.data.DataProvider;
import org.bitbucket.mindwithcake.rssreader.data.FeedItem;
import org.mcsoxford.rss.RSSFault;
import org.mcsoxford.rss.RSSReaderException;

import java.util.ArrayList;

/**
 * An activity representing a list of Feeds. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link FeedDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class FeedListActivity extends AppCompatActivity {

	private ArrayList<FeedItem> mFeeds;

	private RecyclerView mRecyclerView;

	private ListRetrievalTask mTask;

	private FloatingActionButton mFab;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_feed_list);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		if(toolbar != null) {
			setSupportActionBar(toolbar);
			toolbar.setTitle(getTitle());
		}

		View recyclerView = findViewById(R.id.feed_list);
		assert recyclerView != null;
		mRecyclerView = (RecyclerView) recyclerView;

		if(findViewById(R.id.feed_detail_container) != null) {
			mFab = (FloatingActionButton) findViewById(R.id.fab);
		}
	}

	@Override
	protected void onStart() {
		super.onStart();

		mTask = new ListRetrievalTask();
		mTask.execute();
	}

	@Override
	protected void onStop() {
		super.onStop();
		mTask.cancel(true);
	}

	private void setupRecyclerView() {
		mRecyclerView.setAdapter(new FeedAdapter(mFeeds, mFab));
	}

	private class ListRetrievalTask extends AsyncTask<Void, Void, ArrayList<FeedItem>> {
		@Override protected ArrayList<FeedItem> doInBackground(Void... params){
			DataProvider provider = DataProvider.getInstance(FeedListActivity.this);
			try {
				return new ArrayList<>(provider.getFeeds());
			} catch(RSSReaderException | RSSFault e) {
				return null;
			}
		}

		@Override protected void onPostExecute(ArrayList<FeedItem> list){
			if(list != null) {
				mFeeds = list;
				setupRecyclerView();
			}
		}
	}
}
