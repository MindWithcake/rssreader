package org.bitbucket.mindwithcake.rssreader.data;

import org.ksoap2.serialization.SoapObject;

/**
 * Constants containing all the server-related parameters.
 *
 * Created by Ilario Sanseverino on 24/10/15.
 */
public abstract class ServerConstants {
	public static final String FEED_URL = "http://feeds.bbci.co.uk/news/rss.xml";

	public static final String SERVER_URL =
			"http://anaxservices.com:2050/WebServices/wsSaveLingoLangTranslateService.asmx?WSDL";
	public static final String NAMESPACE = "http://tempuri.org/";

	// Methods
	public static final String METHOD_CATEGORIES = "GetContentDataCountsByCategories";
	public static final String METHOD_WORDS = "GetLanguageTranslationDataByCategory";

	// SOAP Actions
	public static final String ACTION_CATEGORIES = NAMESPACE+METHOD_CATEGORIES;
	public static final String ACTION_WORDS = NAMESPACE+METHOD_WORDS;

	// Parameters
	public static final String USER_ID_FIELD = "userID";
	public static final String USER_TYPE_FIELD = "type";
	public static final String CONTENT_TYPE_FIELD = "contentTypeID";

	// Response fields
	public static final String CATEGORY_COUNT = "RecordsCount";
	public static final String CATEGORY_NAME = "categoryName";
	public static final String CATEGORY_ID = "categoryID";
	public static final String CATEGORY_IMAGE = "categoryImagePath";
	public static final String WORD_CONTENT = "langtoSaveContentData";
	public static final String WORD_IMAGE = "imageUrlPath";

	// Constant parameter values
	public static final int USER_ID = 15;
	public static final String USER_TYPE = "curator";
	public static final int CONTENT_TYPE = 1;

	private ServerConstants(){}

	public static SoapObject categoryRequest(){
		SoapObject request = new SoapObject(NAMESPACE, METHOD_CATEGORIES);
		request.addProperty(USER_ID_FIELD, USER_ID);
		request.addProperty(USER_TYPE_FIELD, USER_TYPE);
		request.addProperty(CONTENT_TYPE_FIELD, CONTENT_TYPE);

		return request;
	}

	public static SoapObject wordRequest(long id){
		SoapObject request = new SoapObject(NAMESPACE, METHOD_WORDS);
		request.addProperty(USER_ID_FIELD, USER_ID);
		request.addProperty(USER_TYPE_FIELD, USER_TYPE);
		request.addProperty(CONTENT_TYPE_FIELD, CONTENT_TYPE);
		request.addProperty(CATEGORY_ID, id);
		return request;
	}
}
