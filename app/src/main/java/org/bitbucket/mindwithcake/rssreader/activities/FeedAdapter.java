package org.bitbucket.mindwithcake.rssreader.activities;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.bitbucket.mindwithcake.rssreader.R;
import org.bitbucket.mindwithcake.rssreader.data.FeedItem;

import java.util.List;

public class FeedAdapter extends RecyclerView.Adapter<FeedHolder> {
	private final List<FeedItem> mDataSet;
	private final FloatingActionButton mFab;

	public FeedAdapter(List<FeedItem> dataSet, FloatingActionButton fab){
		mDataSet = dataSet;
		mFab = fab;
	}

	@Override public FeedHolder onCreateViewHolder(ViewGroup parent, int viewType){
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		View view = inflater.inflate(R.layout.view_category, parent, false);
		return new FeedHolder(view, mFab);
	}

	@Override public void onBindViewHolder(FeedHolder holder, int position){
		holder.populate(mDataSet.get(position));
	}

	@Override public int getItemCount(){
		return mDataSet.size();
	}
}
