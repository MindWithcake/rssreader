package org.bitbucket.mindwithcake.rssreader.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.bitbucket.mindwithcake.rssreader.R;
import org.bitbucket.mindwithcake.rssreader.activities.FeedDetailActivity;
import org.bitbucket.mindwithcake.rssreader.activities.FeedDetailFragment;
import org.bitbucket.mindwithcake.rssreader.data.FeedItem;
import org.bitbucket.mindwithcake.rssreader.data.Kandinskij;

/**
 * ViewHolder for the feed adapter.
 *
 * Created by Ilario Sanseverino on 13/10/15.
 */
public class FeedHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
	private final Context mContext;
	@Nullable
	private final FloatingActionButton mFab;
	private final TextView mTextView;
	private final ImageView mImageView;
	private final CardView mCard;
	@SuppressWarnings("FieldCanBeLocal") private Target mTarget;
	private FeedItem mItem;

	public FeedHolder(View itemView, @Nullable FloatingActionButton fab){
		super(itemView);
		mFab = fab;
		mTextView = (TextView)itemView.findViewById(R.id.category_label);
		mImageView = (ImageView)itemView.findViewById(R.id.category_image);
		mCard = (CardView)itemView.findViewById(R.id.category_card);
		mContext = itemView.getContext();
		itemView.setOnClickListener(this);
	}

	public void populate(FeedItem item){
		mTextView.setText(item.headTitle);
		mItem = item;
		if(item.thumbUri != null){
			mTarget = new Target() {
				@Override public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from){
					mImageView.setImageBitmap(bitmap);
					new Palette.Builder(bitmap).generate(new Palette.PaletteAsyncListener() {
						@Override public void onGenerated(Palette palette){
							Palette.Swatch swatch = palette.getVibrantSwatch();
							if(swatch == null)
								swatch = palette.getDarkVibrantSwatch();
							if(swatch == null)
								swatch = palette.getLightMutedSwatch();
							if(swatch == null)
								swatch = palette.getMutedSwatch();

							if (swatch != null) {
								mTextView.setBackgroundColor(swatch.getRgb());
								mTextView.setTextColor(swatch.getTitleTextColor());
								mTextView.getBackground().setAlpha(192);
								mCard.setCardBackgroundColor(swatch.getRgb());
							}
						}
					});
				}

				@Override public void onBitmapFailed(Drawable errorDrawable){
					mImageView.setImageDrawable(errorDrawable);
				}

				@Override public void onPrepareLoad(Drawable placeHolderDrawable){
					mImageView.setImageDrawable(placeHolderDrawable);
				}
			};

			Kandinskij.load(mContext, item.thumbUri).into(mTarget);
		}
	}

	@Override public void onClick(View v){
		if(mFab != null) {
			Bundle arguments = new Bundle();
			arguments.putParcelable(FeedDetailFragment.ARG_ITEM, mItem);
			FeedDetailFragment fragment = new FeedDetailFragment();
			fragment.setArguments(arguments);
			((FragmentActivity)mContext).getSupportFragmentManager()
					.beginTransaction()
					.replace(R.id.feed_detail_container, fragment)
					.commit();
			mFab.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mContext.startActivity(new Intent(Intent.ACTION_VIEW, mItem.link));
				}
			});
		} else {
			Intent intent = new Intent(mContext, FeedDetailActivity.class);
			intent.putExtra(FeedDetailFragment.ARG_ITEM, mItem);
			mContext.startActivity(intent);
		}
	}
}
