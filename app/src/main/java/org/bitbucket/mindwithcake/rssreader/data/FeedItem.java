package org.bitbucket.mindwithcake.rssreader.data;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import org.mcsoxford.rss.RSSItem;

public class FeedItem implements Parcelable {
    public final String headTitle;
    public final Uri link;
    public final String content;
    public Uri thumbUri;

    public FeedItem(RSSItem item){
        headTitle = item.getTitle();
        link = item.getLink();
        content = item.getDescription();
        if(!item.getThumbnails().isEmpty()){
            thumbUri = item.getThumbnails().get(0).getUrl();
        }
    }

    protected FeedItem(Parcel in) {
        headTitle = in.readString();
        link = in.readParcelable(Uri.class.getClassLoader());
        content = in.readString();
        thumbUri = in.readParcelable(Uri.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(headTitle);
        dest.writeParcelable(link, flags);
        dest.writeString(content);
        dest.writeParcelable(thumbUri, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FeedItem> CREATOR = new Creator<FeedItem>() {
        @Override
        public FeedItem createFromParcel(Parcel in) {
            return new FeedItem(in);
        }

        @Override
        public FeedItem[] newArray(int size) {
            return new FeedItem[size];
        }
    };
}
