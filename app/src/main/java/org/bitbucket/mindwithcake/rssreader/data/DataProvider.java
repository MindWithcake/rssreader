package org.bitbucket.mindwithcake.rssreader.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.bitbucket.mindwithcake.rssreader.BuildConfig;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.mcsoxford.rss.RSSFault;
import org.mcsoxford.rss.RSSFeed;
import org.mcsoxford.rss.RSSItem;
import org.mcsoxford.rss.RSSReader;
import org.mcsoxford.rss.RSSReaderException;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.bitbucket.mindwithcake.rssreader.data.ServerConstants.*;

/**
 * This class provides abstract data fetching from the actual backend used. Original data can be a
 * resource file, contained in a local database or got through the web, activities don't have to
 * care about it. Since there is no guarantee that data is immediately available, users must avoid
 * calling method of this class on the main thread.
 * <p/>
 * Created by Ilario Sanseverino on 13/10/15.
 */
public class DataProvider {
	private final static String PREFERENCES_NAME = "RssPrefs";

	private static DataProvider instance;

	private final Context mContext;

	/**
	 * Private constructor for the enforcement of the Singleton design pattern.
	 * @param context the caller Context, used to access resources.
	 */
	private DataProvider(Context context){mContext = context;}

	/**
	 * @param context the caller Context.
	 * @return the singleton instance of this class.
	 */
	public static DataProvider getInstance(Context context){
		if(instance == null)
			instance = new DataProvider(context);

		return instance;
	}

	public List<FeedItem> getFeeds() throws RSSReaderException, RSSFault {
		RSSReader reader = new RSSReader();
		RSSFeed feed = reader.load(FEED_URL);
		List<FeedItem> result = new ArrayList<>(feed.getItems().size());

		for(RSSItem item: feed.getItems()){
			result.add(new FeedItem(item));
		}

		return result;
	}

	private SharedPreferences getPref(){
		return mContext.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
	}

	/**
	 * Internal method to perform an actual request to a .NET web server.
	 *
	 * @param request a SOAPObject with the output parameters.
	 * @param action name of the service to call.
	 * @return a SOAPObject with the server response, or {@code null} if an error occurred.
	 */
	private SoapObject performRequest(SoapObject request, String action){
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.setOutputSoapObject(request);
		envelope.dotNet = true;

		try{
			HttpTransportSE transport = new HttpTransportSE(SERVER_URL);
			transport.call(action, envelope);
			if(envelope.bodyIn instanceof SoapObject)
				return (SoapObject)envelope.bodyIn;
			return null;
		}
		catch(XmlPullParserException | IOException e){
			if(BuildConfig.DEBUG)
				Log.d("Net", "Request error", e);
			return null;
		}
	}
}
