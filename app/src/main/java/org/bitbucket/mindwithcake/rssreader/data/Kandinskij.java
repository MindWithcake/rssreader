package org.bitbucket.mindwithcake.rssreader.data;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;

import com.squareup.picasso.LruCache;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import org.bitbucket.mindwithcake.rssreader.R;

import java.io.File;
import java.util.HashMap;

/**
 * Utility class to initialize the Picasso singleton.
 *
 * Created by Ilario Sanseverino on 18/11/15.
 */
public class Kandinskij {
	private final static int CACHE_SIZE_MB = 60;
	private final static LruCache CACHE = new LruCache(1024 * 1024 * CACHE_SIZE_MB);

	private final static HashMap<String, Typeface> fontCache = new HashMap<>();

	public static Picasso with(Context context){
		return new Picasso.Builder(context)
				.memoryCache(CACHE)
				.downloader(new OkHttpDownloader(context, 1024 * 1024 * 200))
				.build();
	}

	public static RequestCreator load(Context context, String file){
		return fix(with(context).load(file));
	}

	public static RequestCreator load(Context context, Uri file){
		return fix(with(context).load(file));
	}

	public static RequestCreator load(Context context, int resourceID){
		return fix(with(context).load(resourceID));
	}

	public static RequestCreator load(Context context, File file){
		return fix(with(context).load(file));
	}

	public static Typeface getFont(Context context, String fontName){
		Typeface t = fontCache.get(fontName);
		if(t == null){
			t = Typeface.createFromAsset(context.getAssets(), fontName);
			fontCache.put(fontName, t);
		}

		return t;
	}

	private static RequestCreator fix(RequestCreator rc){
		return rc.placeholder(R.drawable.download_placeholder).error(R.drawable.download_error);
	}
}
